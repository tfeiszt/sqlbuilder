<?php
namespace tfeiszt\SqlBuilder\Enum;

/**
 * Class AbstractSqlJoinType
 * @package tfeiszt\SqlBuilder
 */
abstract class AbstractSqlJoinType extends AbstractEnum
{
    /**
     *
     */
    const NORMAL = '';
    /**
     *
     */
    const LEFT = 'LEFT';
    /**
     *
     */
    const RIGHT = 'RIGHT';
    /**
     *
     */
    const INNER = 'INNER';
    /**
     *
     */
    const OUTER = 'OUTER';
}
