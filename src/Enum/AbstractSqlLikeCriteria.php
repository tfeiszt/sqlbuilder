<?php

namespace tfeiszt\SqlBuilder\Enum;

/**
 * Class AbstractSqlLikeCriteria
 * @package tfeiszt\SqlBuilder
 */
abstract class AbstractSqlLikeCriteria extends AbstractEnum
{
    /**
     *
     */
    const CONTAINS = '1';
    /**
     *
     */
    const START_WITH = '2';
    /**
     *
     */
    const ENDS_WIDTH = '3';
    /**
     *
     */
    const EXACT = '4';
}
