<?php
namespace tfeiszt\SqlBuilder;

/**
 * Interface ToSqlInterface
 * @package tfeiszt\SqlBuilder
 */
interface ToSqlInterface
{
    /**
     * @return string
     */
    public function toSql();

    /**
     * @return array
     */
    public function getArgs();

    /**
     * @return int
     */
    public function countArgs();
}
