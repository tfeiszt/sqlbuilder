<?php
namespace tfeiszt\SqlBuilder;

use tfeiszt\SqlBuilder\Operator\AbstractOperator;
use tfeiszt\SqlBuilder\Operator\AndOperator;
use tfeiszt\SqlBuilder\Operator\OrOperator;
use tfeiszt\SqlBuilder\Operator\XorOperator;
use tfeiszt\SqlBuilder\Enum\AbstractSqlLikeCriteria;
use tfeiszt\SqlBuilder\Expression\CustomExpression;
use tfeiszt\SqlBuilder\Expression\ArithmeticExpression;
use tfeiszt\SqlBuilder\Expression\BetweenExpression;
use tfeiszt\SqlBuilder\Expression\LikeExpression;
use tfeiszt\SqlBuilder\Expression\InExpression;
use tfeiszt\SqlBuilder\Expression\NotInExpression;
use tfeiszt\SqlBuilder\Expression\IsExpression;
use tfeiszt\SqlBuilder\Expression\IsNotExpression;
use tfeiszt\SqlBuilder\Expression\ExistsExpression;
use tfeiszt\SqlBuilder\Expression\NotExistsExpression;
use tfeiszt\SqlBuilder\Query\SelectQuery;
use tfeiszt\SqlBuilder\Syntax\SqlList;

/**
 * Class SqlConditions
 * @package tfeiszt\SqlBuilder
 */
class SqlConditions implements ToSqlInterface
{
    /**
     * @var ToSqlInterface[]
     */
    protected $exprs = array();

    /**
     * SqlConditions constructor.
     * @param array $exprs
     */
    public function __construct(array $exprs = array())
    {
        $this->exprs = $exprs;
    }

    /**
     * @param $method
     * @param $args
     * @return SqlConditions
     * @throws \Exception
     */
    public function __call($method, $args)
    {
        switch( $method )
        {
            case 'and':
                $this->exprs[] = new AndOperator;
                break;
            case 'or':
                $this->exprs[] = new OrOperator;
                break;
            case 'xor':
                $this->exprs[] = new XorOperator;
                break;
            default:
                throw new \Exception("Invalid method call: $method");
        }
        return $this;

    }

    /**
     * @param ToSqlInterface $expr
     * @return $this
     */
    public function append(ToSqlInterface $expr)
    {
        if (!empty($this->exprs) && ! end($this->exprs) instanceof AbstractOperator) {
            $this->exprs[] = new AndOperator;
        }
        $this->exprs[] = $expr;
        return $this;
    }

    /**
     * @param $exprStr
     * @param array $args
     * @return $this
     */
    public function add($exprStr, $args = [])
    {
        $this->append(new CustomExpression($exprStr, $args));
        return $this;
    }

    /**
     * @param $a1
     * @param $a2
     * @return $this
     */
    public function equal($a1, $a2)
    {
        $this->append(new ArithmeticExpression($a1, '=', $a2));
        return $this;
    }

    /**
     * @param $a1
     * @param $a2
     * @return $this
     */
    public function notEqual($a1, $a2)
    {
        $this->append(new ArithmeticExpression($a1, '<>', $a2));
        return $this;
    }

    /**
     * @param $a1
     * @param $a2
     * @return $this
     */
    public function greaterThan($a1, $a2)
    {
        $this->append(new ArithmeticExpression($a1, '>', $a2));
        return $this;
    }

    /**
     * @param $a1
     * @param $a2
     * @return $this
     */
    public function greaterThanOrEqual($a1, $a2)
    {
        $this->append(new ArithmeticExpression($a1, '>=', $a2));
        return $this;
    }

    /**
     * @param $a1
     * @param $a2
     * @return $this
     */
    public function lessThan($a1, $a2)
    {
        $this->append(new ArithmeticExpression($a1, '<', $a2));
        return $this;
    }

    /**
     * @param $a1
     * @param $a2
     * @return $this
     */
    public function lessThanOrEqual($a1, $a2)
    {
        $this->append(new ArithmeticExpression($a1, '<=', $a2));
        return $this;
    }

    /**
     * @param $exprStr
     * @param $min
     * @param $max
     * @return $this
     */
    public function between($exprStr, $min, $max)
    {
        $this->append(new BetweenExpression($exprStr, $min, $max));
        return $this;
    }

    /**
     * @param $exprStr
     * @param $pat
     * @param string $criteria
     * @return $this
     */
    public function like($exprStr, $pat, $criteria = AbstractSqlLikeCriteria::CONTAINS)
    {
        $this->append(new LikeExpression($exprStr, $pat, $criteria));
        return $this;
    }

    /**
     * @param $exprStr
     * @param $expr
     * @return $this
     */
    public function in($exprStr, $expr)
    {
        if ($expr instanceof ToSqlInterface) {
            $this->append(new InExpression($exprStr, $expr));

        } else {
            if (! is_array($expr)) {
                $expr = explode(',', $expr);
            }
            $list = new SqlList($expr);
            $this->append(new InExpression($exprStr, $list));
        }
        return $this;
    }

    /**
     * @param $exprStr
     * @param $expr
     * @return $this
     */
    public function notIn($exprStr, $expr)
    {
        if ($expr instanceof ToSqlInterface) {
            $this->append(new NotInExpression($exprStr, $expr));
        } else {
            if (! is_array($expr)) {
                $expr = explode(',', $expr);
            }
            $list = new SqlList($expr);
            $this->append(new NotInExpression($exprStr, $list));
        }
        return $this;
    }

    /**
     * @param $exprStr
     * @param $boolean
     * @return $this
     */
    public function is($exprStr, $boolean) {
        $this->append(new IsExpression($exprStr, $boolean));
        return $this;
    }

    /**
     * @param $exprStr
     * @param $boolean
     * @return $this
     */
    public function isNot($exprStr, $boolean) {
        $this->append(new IsNotExpression($exprStr, $boolean));
        return $this;
    }

    /**
     * @param SelectQuery $expr
     * @return $this
     */
    public function exists(SelectQuery $expr) {
        $this->append(new ExistsExpression($expr));
        return $this;
    }

    /**
     * @param SelectQuery $expr
     * @return $this
     */
    public function notExists(SelectQuery $expr) {
        $this->append(new NotExistsExpression($expr));
        return $this;
    }

    /**
     * @return bool
     */
    public function hasExprs()
    {
        return count($this->exprs) > 0;
    }

    /**
     * @return int
     */
    public function length()
    {
        return count($this->exprs);
    }

    /**
     * @return string
     */
    public function toSql()
    {
        $sql = '';
        foreach ($this->exprs as $expr) {
            if ($expr instanceof SqlConditions) {
                $sql .= ' (' . $expr->toSql() . ')';
            } elseif ($expr instanceof ToSqlInterface) {
                $sql .= ' ' . $expr->toSql();
            } elseif ($expr instanceof AbstractOperator) {
                $sql .= ' ' . $expr->toString();
            } else {
                $sql .= ' ' . $expr;
            }
        }
        return ltrim($sql);
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        $args = [];
        foreach($this->exprs as $expr){
            if (! ($expr instanceof AbstractOperator)) {
                if ($expr->countArgs()) {
                    $a = $expr->getArgs();
                    if (is_array($a)) {
                        foreach ($a as $item) {
                            $args[] = $item;
                        }
                    } else {
                        $args[] = $a;
                    }
                }
            }
        }
        return $args;
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return count($this->getArgs());
    }
}
