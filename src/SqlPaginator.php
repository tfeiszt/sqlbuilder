<?php
namespace tfeiszt\SqlBuilder;

use tfeiszt\SqlBuilder\Syntax\SqlLimit;
use tfeiszt\SqlBuilder\Syntax\SqlOffset;

/**
 * Class SqlPaginator
 * @package tfeiszt\SqlBuilder
 */
class SqlPaginator implements ToSqlInterface
{
    /**
     * @var SqlLimit
     */
    protected $limit;

    /**
     * @var SqlOffset
     */
    protected $offset;

    /**
     * SqlPaginator constructor.
     */
    public function __construct()
    {
        $this->limit = new SqlLimit();
        $this->offset = new SqlOffset();
    }

    /**
     * @param $pageSize
     * @param int $page
     * @return $this
     */
    public function pagination($pageSize, $page = 1)
    {
        if ($page < 1) {
            $page = 1;
        }
        $this->limit->limit((int) $pageSize);
        $this->offset->offset( ((int) $pageSize) * (((int) $page) - 1 ) );
        return $this;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function limit($limit)
    {
        $this->limit->limit((int) $limit);
        return $this;
    }

    /**
     * @param $offset
     * @return $this
     */
    public function offset($offset)
    {
        $this->offset->offset((int) $offset);
        return $this;
    }

    /**
     * @return string
     */
    public function toSql()
    {
        return $this->limit->toSql() . $this->offset->toSql();
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        $args = [];
        foreach($this->limit->getArgs() as $arg){
            $args[] = $arg;
        };
        foreach($this->offset->getArgs() as $arg){
            $args[] = $arg;
        };
        return $args;
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        $args = $this->getArgs();
        return count($args);
    }
}
