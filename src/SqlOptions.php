<?php
namespace tfeiszt\SqlBuilder;

/**
 * Class SqlOptions
 * @package jas\db\sql
 */
class SqlOptions
{
    /**
     * @var bool
     */
    public $distinct = false;

    /**
     * @var bool
     */
    public $distinctRow = false;

    /**
     * @var bool
     */
    public $all = false;

    /**
     * @var bool
     */
    public $countStar = false;

    /**
     * @var string
     */
    public $countStarAlias = '';

    /**
     * @var bool
     */
    public $ignore = false;
}
