<?php
namespace tfeiszt\SqlBuilder\Expression;

use tfeiszt\SqlBuilder\ToSqlInterface;
use tfeiszt\SqlBuilder\Enum\AbstractSqlLikeCriteria;

/**
 * Class LikeExpression
 * @package tfeiszt\SqlBuilder\Expression
 */
class LikeExpression implements ToSqlInterface
{
    /**
     * @var string
     */
    public $operand;

    /**
     * @var string
     */
    public $operand2;

    /**
     * @var array
     */
    public $args = [];

    /**
     * @param $operand
     * @param $operand2
     * @param AbstractSqlLikeCriteria|string $criteria
     */
    public function __construct($operand, $operand2, $criteria = AbstractSqlLikeCriteria::CONTAINS)
    {
        $this->operand = $operand;
        if ($this->operand instanceof ToSqlInterface) {
            if ($operand->countArgs()) {
                $this->args[] = $operand->getArgs();
            }
        }
        $this->operand2 = $operand2;
        $this->args[] = $this->getOperand($operand2, $criteria);

    }

    /**
     * @return string
     */
    public function toSql()
    {
        $sql = '( ';
        if ($this->operand instanceof ToSqlInterface) {
            $sql .= '(' . $this->operand->toSql() .')';
            $sql .= ' LIKE ? ) ';
        } else {
            $sql .= $this->operand . ' LIKE ? )';
        }
        return $sql;
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        return $this->args;
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return count($this->getArgs());
    }

    /**
     * @param $operand
     * @param AbstractSqlLikeCriteria|string $criteria
     * @return string
     */
    private function getOperand($operand, $criteria = AbstractSqlLikeCriteria::CONTAINS)
    {
        if (AbstractSqlLikeCriteria::isValidValue($criteria)) {
            switch ($criteria) {
                case '1' :
                    return '%' . $operand . '%';
                    break;
                case '2' :
                    return $operand . '%';
                    break;
                case '3' :
                    return '%' . $operand;
                    break;
                case '4' :
                    return $operand;
                    break;
            }
        } else {
            throw new \InvalidArgumentException('Invalid AbstractSqlLikeCriteria type');
        }
    }
}
