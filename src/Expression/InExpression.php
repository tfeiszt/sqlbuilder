<?php
namespace tfeiszt\SqlBuilder\Expression;

use tfeiszt\SqlBuilder\ToSqlInterface;

/**
 * Class InExpression
 * @package tfeiszt\SqlBuilder\Expression
 */
class InExpression implements ToSqlInterface
{
    /**
     * @var string
     */
    protected $exprStr;

    /**
     * @var ToSqlInterface
     */
    protected $expr;


    /**
     * @var string
     */
    protected $op = 'IN';


    /**
     * @param $exprStr
     * @param ToSqlInterface $expr
     */
    public function __construct($exprStr, ToSqlInterface $expr)
    {
        $this->exprStr = $exprStr;
        $this->expr = $expr;
    }

    /**
     * @return string
     */
    public function toSql()
    {
        $sql = $this->exprStr . ' ' . $this->op . ' (' . $this->expr->toSql() . ')';
        return '(' . $sql . ')';
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        return $this->expr->getArgs();
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return count($this->getArgs());
    }
}
