<?php
namespace tfeiszt\SqlBuilder\Expression;

use tfeiszt\SqlBuilder\ToSqlInterface;

/**
 * Class NotInExpression
 * @package tfeiszt\SqlBuilder\Expression
 */
class NotInExpression extends InExpression implements ToSqlInterface
{
    protected $op = 'NOT IN';
}
