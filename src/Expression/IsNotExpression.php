<?php
namespace tfeiszt\SqlBuilder\Expression;

use tfeiszt\SqlBuilder\ToSqlInterface;

/**
 * Class IsNotExpression
 * @package tfeiszt\SqlBuilder\Expression
 */
class IsNotExpression extends IsExpression implements ToSqlInterface
{
    protected $op = 'IS NOT';
}
