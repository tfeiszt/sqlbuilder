<?php
namespace tfeiszt\SqlBuilder\Expression;

use tfeiszt\SqlBuilder\ToSqlInterface;

/**
 * Class BetweenExpression
 * @package tfeiszt\SqlBuilder\Expression
 */
class BetweenExpression implements ToSqlInterface
{
    /**
     * @var
     */
    public $exprStr;

    /**
     * @var
     */
    public $min;

    /**
     * @var
     */
    public $max;

    /**
     * @var array
     */
    public $args = [];

    /**
     * @param $exprStr
     * @param $min
     * @param $max
     */
    public function __construct($exprStr, $min, $max)
    {
        $this->exprStr = $exprStr;
        $this->min = $min;
        $this->max = $max;
        $this->args[] = $min;
        $this->args[] = $max;
    }

    /**
     * @return string
     */
    public function toSql()
    {
        return ' ( ' . $this->exprStr . ' BETWEEN ? AND ? ) ';
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        return $this->args;
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return count($this->getArgs());
    }
}
