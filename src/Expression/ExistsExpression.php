<?php
namespace tfeiszt\SqlBuilder\Expression;

use tfeiszt\SqlBuilder\ToSqlInterface;
use tfeiszt\SqlBuilder\Query\SelectQuery;

/**
 * Class ExistsExpression
 * @package tfeiszt\SqlBuilder\Expression
 */
class ExistsExpression implements ToSqlInterface
{
    /**
     * @var SelectQuery
     */
    protected $expr;

    protected $op = 'EXISTS';

    /**
     * @param SelectQuery $expr
     */
    public function __construct(SelectQuery $expr)
    {
        $this->expr = $expr;
    }

    /**
     * @return string
     */
    public function toSql()
    {
        $sql = $this->op . ' (' . $this->expr->toSql() . ')';
        return '(' . $sql . ')';
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        return $this->expr->getArgs();
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return count($this->getArgs());
    }
}
