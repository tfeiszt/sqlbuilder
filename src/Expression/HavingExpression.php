<?php
namespace tfeiszt\SqlBuilder\Expression;

use tfeiszt\SqlBuilder\ToSqlInterface;

/**
 * Class HavingExpression
 * @package tfeiszt\SqlBuilder\Expression
 */
class HavingExpression  extends ArithmeticExpression implements ToSqlInterface
{
    /**
     * @return string
     */
    public function toSql()
    {
        $sql = '(';
        if ($this->operand instanceof ToSqlInterface) {
            $sql .= '(' . $this->operand->toSql() .')';
            $sql .= ' ' . $this->op . ' ? )';
        } else {
            $sql .= $this->operand;
            $sql .= ' ' . $this->op;
            if ($this->operand2 instanceof ToSqlInterface) {
                $sql .= ' (' . $this->operand2->toSql() .'))';
            } else {
                $sql .= ' ? )';
            }
        }

        return $sql;
    }
}
