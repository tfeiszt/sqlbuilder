<?php
namespace tfeiszt\SqlBuilder\Expression;

use tfeiszt\SqlBuilder\ToSqlInterface;

/**
 * Class NotExistsExpression
 * @package tfeiszt\SqlBuilder\Expression
 */
class NotExistsExpression extends ExistsExpression implements ToSqlInterface
{
    protected $op = 'NOT EXISTS';
}
