<?php
namespace tfeiszt\SqlBuilder\Expression;

use tfeiszt\SqlBuilder\ToSqlInterface;

/**
 * Class IsExpression
 * @package tfeiszt\SqlBuilder\Expression
 */
class IsExpression implements ToSqlInterface
{
    /**
     * @var string
     */
    protected $exprStr;

    /**
     * @var string
     */
    protected $boolean;


    /**
     * @var string
     */
    protected $op = 'IS';


    /**
     * @param $exprStr
     * @param null $boolean
     */
    public function __construct($exprStr, $boolean = null)
    {
        $this->exprStr = $exprStr;
        $this->boolean = $boolean;
    }

    /**
     * @return string
     */
    public function toSql()
    {
        $sql = $this->exprStr . ' ' . $this->op . ' ? ';
        return '(' . $sql . ')';
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        return $this->boolean;
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return 1;
    }
}
