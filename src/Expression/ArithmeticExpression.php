<?php
namespace tfeiszt\SqlBuilder\Expression;

use tfeiszt\SqlBuilder\ToSqlInterface;

/**
 * Class ArithmeticExpression
 * @package tfeiszt\SqlBuilder\Expression
 */
class ArithmeticExpression implements ToSqlInterface
{
    /**
     * @var string
     */
    public $op;

    /**
     * @var string
     */
    public $operand;

    /**
     * @var string
     */
    public $operand2;

    /**
     * @var array
     */
    public $args = [];

    /**
     * @param string|ToSqlInterface $operand
     * @param $op
     * @param string|ToSqlInterface $operand2
     */
    public function __construct($operand, $op, $operand2)
    {
        $this->op = $op;
        $this->operand = $operand;
        if ($this->operand instanceof ToSqlInterface) {
            if ($operand->countArgs()) {
                $this->args[] = $operand->getArgs();
            }
        }
        $this->operand2 = $operand2;
        if ($this->operand2 instanceof ToSqlInterface) {
            if ($operand2->countArgs()) {
                $this->args[] = $operand2->getArgs();
            }
        } else {
            $this->args[] = $operand2;
        }
    }

    /**
     * @return string
     */
    public function toSql()
    {
        $sql = '( ';
        if ($this->operand instanceof ToSqlInterface) {
            $sql .= '(' . $this->operand->toSql() .')';
            $sql .= ' ' . $this->op . ' ? ) ';
        } else {
            $sql .= $this->operand;
            $sql .= ' ' . $this->op;
            if ($this->operand2 instanceof ToSqlInterface) {
                $sql .= ' (' . $this->operand2->toSql() .'))';
            } else {
                $sql .= ' ?)';
            }
        }

        return $sql;
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        return $this->args;
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return count($this->getArgs());
    }
}
