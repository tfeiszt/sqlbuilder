<?php
namespace tfeiszt\SqlBuilder\Expression;

use tfeiszt\SqlBuilder\ToSqlInterface;

/**
 * Class CustomExpression
 * @package tfeiszt\SqlBuilder\Expression
 */
class CustomExpression implements ToSqlInterface
{
    /**
     * @var string
     */
    protected $exprStr;

    /**
     * @var array
     */
    protected $args;


    /**
     * @param $exprStr
     * @param array $args
     */
    public function __construct($exprStr, $args = [])
    {
        $this->exprStr = $exprStr;
        $this->args = $args;
    }

    /**
     * @return string
     */
    public function toSql()
    {
        return '(' . $this->exprStr . ')';
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        return $this->args;
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return count($this->args);
    }
}
