<?php
namespace tfeiszt\SqlBuilder\Query;

use tfeiszt\SqlBuilder\ToSqlInterface;
use tfeiszt\SqlBuilder\SqlOptions;
use tfeiszt\SqlBuilder\SqlConditions;

/**
 * Class DeleteQuery
 * @package tfeiszt\SqlBuilder\query
 */
class DeleteQuery implements ToSqlInterface
{
    /**
     * @var string
     */
    protected $table;
    /**
     * @var SqlOptions
     */
    protected $options;
    /**
     * @var SqlConditions
     */
    protected $where;

    /**
     * DeleteQuery constructor.
     */
    public function __construct()
    {
        $this->options = new SqlOptions();
        $this->where = new SqlConditions();
    }

    /**
     * @param $table
     * @return $this
     */
    public function delete($table)
    {
        $this->table = $table;
        return $this;
    }

    /**
     * @return $this
     */
    public function all()
    {
        $this->options->all = true;
        return $this;
    }

    /**
     * @return SqlConditions
     */
    public function where()
    {
        return $this->where;
    }

    /**
     * @param SqlConditions $conditions
     * @return $this
     */
    public function setConditions(SqlConditions $conditions)
    {
        unset($this->where);
        $this->where = $conditions;
        return $this;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function buildWhereClause()
    {
        $sql = $this->where->toSql();
        if (($sql != '') || ($this->options->all === true)) {
            return ($sql != '') ? (' WHERE ' . addslashes($sql)) : addslashes($sql);
        } else {
            throw new \Exception('Empty conditions on DELETE query is not allowed. Use ALL option.');
        }
    }

    /**
     * @return string
     */
    public function toSql() {

        $sql = 'DELETE FROM ' .  addslashes($this->table) . ' ';

        $sql .= $this->buildWhereClause();

        return $sql;
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        $args = [];
        foreach($this->where()->getArgs() as $arg) {
            $args[] = $arg;
        }
        return $args;
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return count($this->getArgs());
    }
}
