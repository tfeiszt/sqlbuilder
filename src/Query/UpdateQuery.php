<?php
namespace tfeiszt\SqlBuilder\Query;

use tfeiszt\SqlBuilder\ToSqlInterface;
use tfeiszt\SqlBuilder\SqlOptions;
use tfeiszt\SqlBuilder\SqlConditions;

/**
 * Class UpdateQuery
 * @package tfeiszt\SqlBuilder\Query
 */
class UpdateQuery implements ToSqlInterface
{
    /**
     * @var string
     */
    protected $table;
    /**
     * @var array
     */
    protected $values = array();
    /**
     * @var SqlOptions
     */
    protected $options;
    /**
     * @var SqlConditions
     */
    protected $where;

    /**
     * UpdateQuery constructor.
     */
    public function __construct()
    {
        $this->options = new SqlOptions();
        $this->where = new SqlConditions();
    }

    /**
     * @param $name
     * @param null $value
     * @return $this
     */
    public function set($name, $value = null)
    {
        $this->values[$name] = $value;
        return $this;
    }

    /**
     * @param array $values
     * @return $this
     */
    public function setValues(array $values)
    {
        $this->values = $values;
        return $this;
    }

    /**
     * @param $table
     * @return $this
     */
    public function update($table)
    {
        $this->table = $table;
        return $this;
    }

    /**
     * @return $this
     */
    public function all()
    {
        $this->options->all = true;
        return $this;
    }

    /**
     * @return SqlConditions
     */
    public function where()
    {
        return $this->where;
    }

    /**
     * @param SqlConditions $conditions
     * @return $this
     */
    public function setConditions(SqlConditions $conditions)
    {
        unset($this->where);
        $this->where = $conditions;
        return $this;
    }

    /**
     * @return array
     */
    public function getColumnNames() {
        return array_keys($this->values);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function buildWhereClause()
    {
        $sql = $this->where->toSql();
        if (($sql != '') || ($this->options->all === true)) {
            return ($sql != '') ? (' WHERE ' . addslashes($sql)) : addslashes($sql);
        } else {
            throw new \Exception('Empty conditions on UPDATE query is not allowed. Use ALL option.');
        }
    }

    /**
     * @return string
     */
    public function toSql() {
        $sql = 'UPDATE ' . addslashes($this->table) . ' SET ';

        $values = [];
        foreach($this->values as $k => $value){
            if ($value instanceof ToSqlInterface) {
                $values[] = $k . ' = (' . $value->toSql() . ')';
            } else {
                $values[] = $k . ' = ?';
            }
        }

        $sql .= join(', ',$values);

        $sql .= $this->buildWhereClause();

        return $sql;
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        $args = [];
        foreach($this->values as $k => $value){
            if ($value instanceof ToSqlInterface) {
                if ($value->countArgs() > 0) {
                    foreach($value->getArgs() as $arg) {
                        $args[] = $arg;
                    }
                }
            } else {
                $args[] = $value;
            }
        }
        foreach($this->where()->getArgs() as $arg) {
            $args[] = $arg;
        }
        return $args;
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return count($this->getArgs());
    }
}
