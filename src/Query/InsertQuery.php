<?php
namespace tfeiszt\SqlBuilder\Query;

use tfeiszt\SqlBuilder\ToSqlInterface;
use tfeiszt\SqlBuilder\SqlOptions;

/**
 * Class InsertQuery
 * @package tfeiszt\SqlBuilder\Query
 */
class InsertQuery implements ToSqlInterface
{
    /**
     * @var string
     */
    protected $table;
    /**
     * @var array
     */
    protected $values = array();
    /**
     * @var SqlOptions
     */
    protected $options;

    /**
     * InsertQuery constructor.
     */
    public function __construct()
    {
        $this->options = new SqlOptions();
    }

    /**
     * @param $name
     * @param null $value
     * @return $this
     */
    public function insert($name, $value = null)
    {
        $this->values[$name] = $value;
        return $this;
    }

    /**
     * @param array $values
     * @return $this
     */
    public function insertValues(array $values)
    {
        $this->values = $values;
        return $this;
    }

    /**
     * @return $this
     */
    public function ignore()
    {
        $this->options->ignore = true;
        return $this;
    }

    /**
     * @param $table
     * @return $this
     */
    public function into($table)
    {
        $this->table = $table;
        return $this;
    }

    /**
     * @return array
     */
    public function getColumnNames() {
        return array_keys($this->values);
    }


    /**
     * @return string
     */
    public function toSql() {
        $sql = 'INSERT';

        if ($this->options->ignore === true) {
            $sql .= ' IGNORE';
        }

        $sql .= ' INTO ' .$this->table;

        $columns = $this->getColumnNames();

        $values = [];
        foreach($this->values as $k => $value){
            if ($value instanceof ToSqlInterface) {
                $values[] = '(' . $value->toSql() . ')';
            } else {
                $values[] = '?';
            }
        }

        $sql .= ' (' . join(', ',$columns) . ')'
            . ' VALUES (' . join(', ', $values) .')';

        return $sql;
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        $args = [];
        foreach($this->values as $k => $value){
            if ($value instanceof ToSqlInterface) {
                if ($value->countArgs() > 0) {
                    foreach($value->getArgs() as $arg) {
                        $args[] = $arg;
                    }
                }
            } else {
                $args[] = $value;
            }
        }
        return $args;
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return count($this->getArgs());
    }
}
