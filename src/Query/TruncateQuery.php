<?php
namespace tfeiszt\SqlBuilder\Query;

use tfeiszt\SqlBuilder\ToSqlInterface;

/**
 * Class TruncateQuery
 * @package tfeiszt\SqlBuilder\Query
 */
class TruncateQuery implements ToSqlInterface
{
    /**
     * @var string
     */
    protected $table;

    /**
     * @param $table
     * @return $this
     */
    public function truncate($table)
    {
        $this->table = $table;
        return $this;
    }

    /**
     * @return string
     */
    public function toSql() {

        $sql = 'TRUNCATE ' . $this->table;

        return $sql;
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        return [];
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return 0;
    }
}
