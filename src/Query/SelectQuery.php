<?php
namespace tfeiszt\SqlBuilder\Query;

use tfeiszt\SqlBuilder\ToSqlInterface;
use tfeiszt\SqlBuilder\SqlOptions;
use tfeiszt\SqlBuilder\SqlConditions;
use tfeiszt\SqlBuilder\SqlPaginator;
use tfeiszt\SqlBuilder\Enum\AbstractSqlJoinType;

use tfeiszt\SqlBuilder\Expression\HavingExpression;

use tfeiszt\SqlBuilder\Syntax\SqlOrderBy;
use tfeiszt\SqlBuilder\Syntax\SqlGroupConditions;
use tfeiszt\SqlBuilder\Syntax\SqlJoin;

/**
 * Class SelectQuery
 * @package tfeiszt\SqlBuilder\Query
 */
class SelectQuery implements ToSqlInterface
{
    /**
     * @var string
     */
    public $alias = '';
    /**
     * @var SqlOptions
     */
    protected $options;
    /**
     * @var SelectQuery[]
     */
    protected $unions = array();
    /**
     * @var array
     */
    protected $select = array();
    /**
     * @var array
     */
    protected $from = array();
    /**
     * @var SqlJoin[]
     */
    protected $joins = array();
    /**
     * @var SqlConditions
     */
    protected $where;
    /**
     * @var SqlGroupConditions
     */
    protected $group;
    /**
     * @var SqlConditions
     */
    protected $having;
    /**
     * @var SqlOrderBy
     */
    protected $order;
    /**
     * @var SqlPaginator
     */
    protected $paging;

    /**
     * SelectQuery constructor.
     * @param null|string|array $select
     */
    public function __construct($select = null)
    {
        $this->options = new SqlOptions();
        $this->where = new SqlConditions();
        $this->group = new SqlGroupConditions();
        $this->having = new SqlConditions();
        $this->paging = new SqlPaginator();
        $this->order = new SqlOrderBy();
        if ($select !== null) {
            $this->select($select);
        }
    }


    /**********************************************************
     * Accessors
     **********************************************************/

    public function distinct()
    {
        $this->options->distinct = true;
        return $this;
    }

    /**
     * @return $this
     */
    public function distinctRow()
    {
        $this->options->distinctRow = true;
        return $this;
    }

    /**
     * @return $this
     */
    public function all()
    {
        $this->options->all = true;
        return $this;
    }

    /**
     * @param string $alias
     * @return $this
     */
    public function selectCountStar($alias = '')
    {
        $this->options->countStar = true;
        $this->options->countStarAs = $alias;
        $this->select = array();
        return $this;
    }

    /**
     * @param $select
     * @return $this
     */
    public function select($select)
    {
        if (is_array($select)) {
            foreach($select as $k => $item) {
                if (is_integer($k)) {
                    $this->select[] = $item;
                } else {
                    $this->select[] = $select;
                }
            }
        } else {
            $tmp = func_get_args();
            if (is_array($tmp)) {
                foreach($tmp as $item){
                    $this->select[] = $item;
                }
            }
        }
        return $this;
    }

    /**
     * @param $select
     * @return $this
     */
    public function setSelect($select)
    {
        if (is_array($select)) {
            $this->select = $select;
        } else {
            $this->select = func_get_args();
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getSelect()
    {
        return $this->select;
    }

    /**
     * @param string|ToSqlInterface $table
     * @param null $alias
     * @return $this
     */
    public function from($table, $alias = NULL)
    {
        if ($table instanceof SelectQuery) {
            if ($alias) {
                $table->alias = $alias;
                $this->from[] = $table;
            } else {
                new \Exception('Dynamic view must have alias');
            }
        } else {
            if ($alias) {
                $this->from[$table] = $alias;
            } else {
                $this->from[] = $table;
            }
        }
        return $this;
    }

    /**
     * @param $table
     * @return $this
     */
    public function setFrom($table)
    {
        if (is_array($table)) {
            $this->from = $table;
        } else {
            $this->from = func_get_args();
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getFrom()
    {
        return $this->from;
    }


    /**
     * @return SqlConditions
     */
    public function where()
    {
        return $this->where;
    }

    /**
     * @param SqlConditions $conditions
     * @return $this
     */
    public function setConditions(SqlConditions $conditions)
    {
        unset($this->where);
        $this->where = $conditions;
        return $this;
    }

    /**
     * @param SelectQuery $query
     * @return $this
     */
    public function union(SelectQuery $query)
    {
        $this->unions[] = $query;
        return $this;
    }

    /********************************************************
     * HAVING clauses
     *
     *******************************************************/

    /**
     * @param mixed|ToSqlInterface $a1
     * @param string $op
     * @param mixed|ToSqlInterface $a2
     * @return $this
     */
    public function having($a1, $op, $a2)
    {
        $this->having->append(new HavingExpression($a1, $op, $a2));

        return $this;
    }

    /**
     * @param SqlConditions $conditions
     * @return $this
     */
    public function setHavingConditions(SqlConditions $conditions )
    {
        $this->having = $conditions;
        return $this;
    }

    /**
     * @return SqlConditions
     */
    public function getHavingConditions()
    {
        return $this->having;
    }

    /********************************************************
     * ORDER BY clauses
     *
     *******************************************************/
    /**
     * @param $byExpr
     * @param null $sorting
     * @return $this
     */
    public function orderBy($byExpr, $sorting = null)
    {
        $this->order->orderBy($byExpr, $sorting);
        return $this;
    }

    /**
     * @param array $arguments
     * [
     *   [ 'column_name', 'ASC' ]
     * ]
     *
     * @return $this
     */
    public function setOrderByList($arguments = array())
    {
        $this->order->clearOrderByList();
        $this->order->setOrderByList($arguments);
        return $this;
    }

    /**
     * @return array
     */
    public function getOrderByList()
    {
        return $this->order->getArgs();
    }


    /********************************************************
     * LIMIT and OFFSET clauses
     *
     *******************************************************/
    /**
     * @param int $limit
     * @return $this
     */
    public function limit($limit)
    {
        $this->paging->limit($limit);
        return $this;
    }

    /**
     * @param int $offset
     * @return $this
     */
    public function offset($offset)
    {
        $this->paging->offset($offset);
        return $this;
    }

    /**
     * @param int $pageSize
     * @param int $offset
     * @return $this
     */
    public function page($pageSize, $offset = 0)
    {
        $this->paging->pagination($pageSize, $offset);
        return $this;
    }

    /********************************************************
     * GROUP BY clauses
     *
     *******************************************************/
    /**
     * @param string|ToSqlInterface $expr
     * @return $this
     */
    public function groupBy($expr)
    {
        if (is_array($expr)) {
            $this->group->setGroupByList($expr);
        } else {
            $this->group->groupBy($expr);
        }
        return $this;
    }

    /**
     * @param array $expr
     * [
     *   'column_name_1', 'column_name_2', ToSqlInterface (subselect)
     * ]
     *
     * @return $this
     */
    public function setGroupByList(array $expr = array())
    {
        $this->group->clearGroupByList();
        $this->group->setGroupByList($expr);
        return $this;
    }

    /**
     *
     */
    public function clearGroupBy()
    {
        $this->group->clearGroupByList();
    }


    /****************************************************************
     * Joins
     ***************************************************************/
    public function innerJoin($table, $alias = NULL)
    {
        $join = new SqlJoin($table, $alias, AbstractSqlJoinType::INNER);
        $join->setQuery($this);
        $this->joins[] = $join;
        return $join;
    }

    /**
     * @param $table
     * @param null $alias
     * @return SqlJoin
     */
    public function outerJoin($table, $alias = NULL)
    {
        $join = new SqlJoin($table, $alias, AbstractSqlJoinType::OUTER);
        $join->setQuery($this);
        $this->joins[] = $join;
        return $join;
    }

    /**
     * @param $table
     * @param null $alias
     * @return SqlJoin
     */
    public function rightJoin($table, $alias = NULL)
    {
        $join = new SqlJoin($table, $alias, AbstractSqlJoinType::RIGHT);
        $join->setQuery($this);
        $this->joins[] = $join;
        return $join;
    }

    /**
     * @param $table
     * @param null $alias
     * @return SqlJoin
     */
    public function leftJoin($table, $alias = NULL)
    {
        $join = new SqlJoin($table, $alias, AbstractSqlJoinType::LEFT);
        $join->setQuery($this);
        $this->joins[] = $join;
        return $join;
    }

    /**
     * @param $table
     * @param null $alias
     * @param string $joinType
     * @return SqlJoin
     */
    public function join($table, $alias = NULL, $joinType = AbstractSqlJoinType::NORMAL)
    {
        $join = new SqlJoin($table, $alias, $joinType);
        $join->setQuery($this);
        $this->joins[] = $join;
        return $join;
    }

    /****************************************************************
     * Builders
     ***************************************************************/
    public function buildSelectClause() {
        $sql = ' ';
        $first = true;
        if ($this->options->countStar === true) {
            $sql .= 'COUNT(*)';
            if ($this->options->countStarAs != '') {
                $sql .= ' AS ' . $this->options->countStarAs;
            }
        } else {
            if ($this->options->distinct === true) {
                $sql .=' DISTINCT ';
            }
            if ($this->options->distinctRow === true) {
                $sql .=' DISTINCTROW ';
            }
            foreach ($this->select as $k => $v) {
                if ($first) {
                    $first = false;
                } else {
                    $sql .= ', ';
                }
                /* "column AS alias" OR just "column" */
                if (is_integer($k)) {
                    if (is_array($v)) {
                        if (count($v) == 2) {
                            if ($v[0] instanceof SelectQuery || $v[0] instanceof ToSqlInterface) {
                                $sql .= $v[0]->toSql() . ' AS ' . $v[1];
                            } else {
                                $sql .= $v[0] . ' AS ' . $v[1];
                            }
                        } else {
                            if (array_keys($v) !== range(0, count($v) - 1)) {
                                $sql .= array_keys($v)[0] . ' AS ' . $v[array_keys($v)[0]];
                            } else {
                                if ($v[0] instanceof SelectQuery || $v[0] instanceof ToSqlInterface) {
                                    $sql .= $v[0]->toSql();
                                } else {
                                    $sql .= $v[0];
                                }
                            }
                        }
                    } else {
                        if ($v instanceof SelectQuery || $v instanceof ToSqlInterface) {
                            $sql .= $v->toSql();
                        } else {
                            $sql .= $v;
                        }
                    }
                } else {
                    if ($v instanceof SelectQuery || $v instanceof ToSqlInterface) {
                        $sql .= '( ' . $v->toSql() . ') AS ' . $k;
                    } else {
                        $sql .= $k . ' AS ' . $v;
                    }
                }
            }
        }
        return $sql;
    }

    /**
     * @return string
     */
    public function buildFromClause() {
        $tableRefs = array();
        foreach($this->from as $k => $v) {
            /* "column AS alias" OR just "column" */
            if (is_string($k)) {
                $sql = $k . ' ' . $v;
                $tableRefs[] = $sql;
            } elseif ( is_integer($k) || is_numeric($k) ) {
                if ($v instanceof SelectQuery) {
                    $sql = ' (' . $v->toSql() . ') ' . $v->alias . ' ';
                } else {
                    $sql = $v;
                }
                $tableRefs[] = $sql;
            }
        }
        if (!empty($tableRefs)) {
            return ' FROM ' . addslashes(join(', ', $tableRefs));
        }
        return '';
    }

    /**
     * @return string
     */
    public function buildJoinClause()
    {
        $sql = '';
        if ($this->joins && is_array($this->joins)) {
            foreach ($this->joins as $join) {
                $sql .= $join->toSql();
            }
        }
        return $sql;
    }

    /**
     * @return string
     */
    public function buildGroupByClause() {
        return $this->group->toSql();
    }

    /**
     * @return string
     */
    public function buildLimitClause()
    {
        return $this->paging->toSql();
    }

    /**
     * @return string
     */
    public function buildHavingClause()
    {
        if ($this->having->hasExprs()) {
            return ' HAVING ' . $this->having->toSql();
        }
        return '';
    }

    /**
     * @return string
     */
    public function buildWhereClause()
    {
        $conditions = $this->where->toSql();
        return (trim($conditions) != '') ? ' WHERE ' . addslashes($conditions) : '';
    }

    /**
     * @return string
     */
    public function buildUnionQueries()
    {
        $sql = '';
        foreach ($this->unions as $union){
            $sql .= ' UNION ';
            if ($this->options->all === true) {
                $sql .='ALL ';
            }
            $sql .= $union->toSql();
        }
        return $sql;
    }

    /**
     * @return string
     */
    public function buildOrderByClause()
    {
        return $this->order->toSql();
    }

    /**
     * @return string
     */
    public function toSql() {
        $sql = 'SELECT'
            . $this->buildSelectClause()
            . $this->buildFromClause()
            . $this->buildJoinClause()
            . $this->buildWhereClause()
            . $this->buildGroupByClause()
            . $this->buildHavingClause()
            . $this->buildUnionQueries()
            . $this->buildOrderByClause()
            . $this->buildLimitClause()
        ;
        return $sql;
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        $args = [];
        foreach($this->from as $k => $v) {
            if ($v instanceof SelectQuery) {
                foreach ($v->getArgs() as $arg) {
                    $args[] = $arg;
                }
            }
        }
        foreach ($this->joins as $join) {
            if ($join->countArgs() > 0) {
                foreach ($join->getArgs() as $arg) {
                    $args[] = $arg;
                }
            }
        }
        foreach($this->where()->getArgs() as $arg) {
            $args[] = $arg;
        }
        foreach ($this->having->getArgs() as $arg) {
            $args[] = $arg;
        };
        foreach ($this->unions as $union){
            foreach($union->getArgs() as $arg) {
                $args[] = $arg;
            }
        }
        foreach($this->paging->getArgs() as $arg) {
            $args[] = $arg;
        }
        return $args;
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return count($this->getArgs());
    }
}
