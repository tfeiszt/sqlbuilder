<?php
namespace tfeiszt\SqlBuilder\Operator;

/**
 * Class NotOperator
 * @package tfeiszt\SqlBuilder\Operator
 */
class NotOperator extends AbstractOperator
{
    /**
     * @return string
     */
    public function toString() {
        return 'NOT';
    }
}
