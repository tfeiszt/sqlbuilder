<?php
namespace tfeiszt\SqlBuilder\Operator;

/**
 * Class OrOperator
 * @package tfeiszt\SqlBuilder\Operator
 */
class OrOperator extends AbstractOperator
{
    /**
     * @return string
     */
    public function toString() {
        return 'OR';
    }
}

