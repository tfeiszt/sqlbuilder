<?php
namespace tfeiszt\SqlBuilder\Operator;

/**
 * Class XorOperator
 * @package tfeiszt\SqlBuilder\Operator
 */
class XorOperator extends AbstractOperator
{
    /**
     * @return string
     */
    public function toString() {
        return 'XOR';
    }
}
