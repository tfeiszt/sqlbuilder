<?php
namespace tfeiszt\SqlBuilder\Operator;

/**
 * Class AndOperator
 * @package tfeiszt\SqlBuilder\Operator
 */
class AndOperator extends AbstractOperator
{
    /**
     * @return string
     */
    public function toString() {
        return 'AND';
    }
}
