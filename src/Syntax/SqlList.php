<?php
namespace tfeiszt\SqlBuilder\Syntax;

use tfeiszt\SqlBuilder\ToSqlInterface;

/**
 * Class SqlList
 * @package tfeiszt\SqlBuilder\Syntax
 */
class SqlList implements ToSqlInterface
{
    /**
     * @var array
     */
    protected $args;

    /**
     * @param array $args
     */
    public function __construct($args = array())
    {
        $this->args = $args;
    }

    /**
     * @return string
     */
    public function toSql()
    {
        $sql = '';
        if ($this->countArgs()) {
            foreach($this->args as $arg) {
                $sql .= ', ?';
            }
            $sql = ltrim($sql, ',');
        }
        return $sql;
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        return $this->args;
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return count($this->args);
    }
}
