<?php
namespace tfeiszt\SqlBuilder\Syntax;

use tfeiszt\SqlBuilder\ToSqlInterface;

/**
 * Class SqlGroupConditions
 * @package tfeiszt\SqlBuilder\Syntax
 */
class SqlGroupConditions implements ToSqlInterface
{
    /**
     * [
     *    'column_name_1', 'column_name_2', ToSqlInterface (subselect)
     * ]
     */
    protected $groupByList = array();

    /**
     * @param string|ToSqlInterface $byExpr
     * @return $this
     */
    public function groupBy($byExpr)
    {
        $this->groupByList[] = $byExpr;
        return $this;
    }

    /**
     *
     */
    public function clearGroupByList()
    {
        $this->groupByList = array();
    }

    /**
     * @param string[]|ToSqlInterface[] $groupBy
     * @return $this
     */
    public function setGroupByList(array $groupBy)
    {
        $this->groupByList = $groupBy;
        return $this;
    }

    /**
     * @return string
     */
    public function toSql()
    {
        if (empty($this->groupByList)) {
            return '';
        }

        $sql = '';
        foreach($this->groupByList as $groupBy) {
            if (is_string($groupBy)) {
                $sql .= ', ' . $groupBy;

            } elseif ($groupBy instanceof ToSqlInterface) {
                $sql .= ', ' . $groupBy->toSql();
            }
        }
        return ' GROUP BY' . ltrim($sql, ',');
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        $args = [];
        if (! empty($this->groupByList)) {
            foreach ($this->groupByList as $groupBy) {
                if ($groupBy instanceof ToSqlInterface) {
                    if ($groupBy->countArgs()) {
                        $args = $groupBy->getArgs();
                    }
                }
            }
        }
        return $args;
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return count($this->getArgs());
    }
}
