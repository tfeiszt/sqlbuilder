<?php
namespace tfeiszt\SqlBuilder\Syntax;

use tfeiszt\SqlBuilder\ToSqlInterface;

/**
 * Class SqlOffset
 * @package tfeiszt\SqlBuilder\Syntax
 */
class SqlOffset implements ToSqlInterface
{
    /**
     * @var integer
     */
    protected $offset;

    /**
     * @param integer $offset
     * @return $this
     */
    public function offset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    /**
     * @return string
     */
    public function toSql()
    {
        if ($this->offset) {
            return ' OFFSET ' . (int) $this->offset . ' ';
        }
        return '';
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        return [];
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return count($this->getArgs());
    }
}
