<?php
namespace tfeiszt\SqlBuilder\Syntax;

use tfeiszt\SqlBuilder\ToSqlInterface;
use tfeiszt\SqlBuilder\SqlConditions;
use tfeiszt\SqlBuilder\Enum\AbstractSqlJoinType;
use tfeiszt\SqlBuilder\query\SelectQuery;

/**
 * Class SqlJoin
 * @package tfeiszt\SqlBuilder\Syntax
 */
class SqlJoin implements ToSqlInterface
{
    /**
     * @var SelectQuery
     */
    public $query;
    /**
     * @var string|SelectQuery
     */
    public $table;
    /**
     * @var string
     */
    public $alias;
    /**
     * @var SqlConditions
     */
    public $conditions;
    /**
     * @var AbstractSqlJoinType|string
     */
    protected $joinType;

    /**
     * @param $table
     * @param null $alias
     * @param string $joinType
     */
    public function __construct($table, $alias = NULL, $joinType = AbstractSqlJoinType::NORMAL)
    {
        $this->table = $table;
        $this->alias = $alias;
        $this->joinType = $joinType;
        $this->conditions = new SqlConditions();
    }

    public function setQuery(SelectQuery $query)
    {
        $this->query = $query;
    }

    public function left() {
        $this->joinType = AbstractSqlJoinType::LEFT;
        return $this;
    }

    public function right() {
        $this->joinType = AbstractSqlJoinType::RIGHT;
        return $this;
    }

    public function inner() {
        $this->joinType = AbstractSqlJoinType::INNER;
        return $this;
    }

    public function outer() {
        $this->joinType = AbstractSqlJoinType::OUTER;
        return $this;
    }

    /**
     * @param string|ToSqlInterface $conditionExpr
     * @param array $args
     * @return SqlConditions
     */
    public function on($conditionExpr, array $args = [])
    {
        if (is_string($conditionExpr)){
            $this->conditions->add($conditionExpr, $args);
        } else {
            $this->conditions->append($conditionExpr);
        }
        return ($this) ? $this : null;
    }


    /**
     * @param SqlConditions $conditions
     * @return SqlConditions
     */
    public function setConditions(SqlConditions $conditions)
    {
        $this->conditions = $conditions;
        return $conditions;
    }

    /**
     * @return string
     */
    public function toSql()
    {
        $sql = '';

        if ($this->joinType) {
            $sql .= ' ' . $this->joinType;
        }

        if ($this->table instanceof SelectQuery) {
            $sql .= ' JOIN (' . $this->table->toSql() . ')';

            if ($this->alias) {
                $sql .= ' ' . $this->alias;
            }

        } else {
            $sql .= ' JOIN ' . $this->table;

            if ($this->alias) {
                $sql .= ' ' . $this->alias;
            }
        }

        if ($this->conditions->hasExprs()) {
            $sql .= ' ON (' . $this->conditions->toSql() . ')';
        }
        return $sql;
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        if ($this->table instanceof SelectQuery) {
            if ($this->table->countArgs() > 0) {
                $args = $this->table->getArgs();
                foreach($this->conditions->getArgs() as $arg) {
                    $args[] = $arg;
                }
            } else {
                $args = $this->conditions->getArgs();
            }
        } else {
            $args = $this->conditions->getArgs();
        }
        return $args;
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return count($this->getArgs());
    }
}
