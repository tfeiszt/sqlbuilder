<?php
namespace tfeiszt\SqlBuilder\Syntax;

use tfeiszt\SqlBuilder\ToSqlInterface;

/**
 * Class SqlOrderBy
 * @package tfeiszt\SqlBuilder\Syntax
 */
class SqlOrderBy implements ToSqlInterface
{
    /**
     * [
     *   [ 'column_name', 'ASC' ]
     * ]
     */
    protected $orderByList = array();

    /**
     * @param $byExpr
     * @param null $sorting
     * @return $this
     */
    public function orderBy($byExpr, $sorting = NULL)
    {
        $this->orderByList[] = array($byExpr, $sorting);
        return $this;
    }

    /**
     *
     */
    public function clearOrderByList()
    {
        $this->orderByList = array();
    }

    /**
     * @param array $orderBy
     * @return $this
     */
    public function setOrderByList(array $orderBy)
    {
        $this->orderByList = $orderBy;
        return $this;
    }

    /**
     * @return array
     */
    public function getOrderByList()
    {
        return $this->orderByList;
    }

    /**
     * @return string
     */
    public function toSql()
    {
        if (empty($this->orderByList)) {
            return '';
        }

        $sql = '';
        foreach($this->orderByList as $orderBy) {
            if (is_string($orderBy[0])) {
                $sql .= ', ' . $orderBy[0];
                if (isset($orderBy[1]) && $orderBy[1]) {
                    $sql .= ' ' . $orderBy[1];
                }
            } elseif ($orderBy[0] instanceof ToSqlInterface) {
                $sql .= ', ' . $orderBy[0]->toSql();
                if (isset($orderBy[1]) && $orderBy[1]) {
                    $sql .= ' ' . $orderBy[1];
                }
            }
        }
        return ' ORDER BY' . addslashes(ltrim($sql, ','));
    }


    /**
     * @return array
     */
    public function getArgs()
    {
        $args = [];
        foreach($this->orderByList as $orderBy) {
            if ($orderBy[0] instanceof ToSqlInterface) {
                if ($orderBy[0]->countArgs()) {
                    $a = $orderBy[0]->getArgs();
                    if (is_array($a)) {
                        foreach ($a as $item) {
                            $args[] = $item;
                        }
                    } else {
                        $args[] = $a;
                    }
                }
            }
        }
        return $args;
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return count($this->getArgs());
    }
}
