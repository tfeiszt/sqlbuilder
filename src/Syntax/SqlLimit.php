<?php
namespace tfeiszt\SqlBuilder\Syntax;

use tfeiszt\SqlBuilder\ToSqlInterface;

/**
 * Class SqlLimit
 * @package tfeiszt\SqlBuilder\Syntax
 */
class SqlLimit implements ToSqlInterface
{
    /**
     * @var integer
     */
    protected $limit;

    /**
     * @param int $limit
     * @return $this
     */
    public function limit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @return string
     */
    public function toSql()
    {
        if ($this->limit) {
            return ' LIMIT ' . (int) $this->limit . ' ';
        }
        return '';
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        return [];
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return count($this->getArgs());
    }
}
