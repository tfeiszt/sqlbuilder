<?php
namespace tfeiszt\SqlBuilder\Syntax;

use tfeiszt\SqlBuilder\ToSqlInterface;

/**
 * Class SqlCount
 * @package tfeiszt\SqlBuilder\Syntax
 */
class SqlCount implements ToSqlInterface
{
    /**
     * @var string
     */
    protected $count;
    /**
     * @var string
     */
    protected $alias;

    /**
     * @param string $count
     * @param string $alias
     */
    public function __construct($count = '*', $alias = '')
    {
        $this->count = $count;
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function toSql()
    {
        if ($this->count) {
            return ' COUNT(' . $this->count . ')' . (($this->alias != '') ? (' AS ' . $this->alias) : '');
        }
        return '';
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        return [];
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return 0;
    }
}
