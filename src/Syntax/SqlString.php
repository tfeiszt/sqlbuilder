<?php
namespace tfeiszt\SqlBuilder\Syntax;

use tfeiszt\SqlBuilder\ToSqlInterface;

/**
 * Class SqlString
 * @package tfeiszt\SqlBuilder\Syntax
 */
class SqlString implements ToSqlInterface
{
    /**
     * @var string
     */
    protected $sql;

    /**
     * @var array
     */
    protected $args;

    /**
     * @param $sql
     * @param array $args
     */
    public function __construct($sql, $args = [])
    {
        $this->sql = $sql;
        $this->args = $args;
    }

    /**
     * @return string
     */
    public function toSql()
    {
        return $this->sql;
    }

    /**
     * @return array
     */
    public function getArgs()
    {
        return $this->args;
    }

    /**
     * @return int
     */
    public function countArgs()
    {
        return count($this->args);
    }
}
