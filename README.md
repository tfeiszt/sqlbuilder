# tfeiszt SqlBuilder  

### PHP library description
Object oriented general SQL builder.
 
### Syntax
  * MySql
  * PostgreSQL

### Supported queries
  * Select
  * Update
  * Delete
  * Insert
  * Truncate
  * Union
  * Sub-select as field
  * Sub-select as condition
  * Sub-select as source (dynamic view)
  
### Install by composer
```php
{
  "repositories": [
    {
      "type": "vcs",
      "url":  "https://tfeiszt@bitbucket.org/tfeiszt/sqlbuilder.git"
    }
  ],
  "require": {
    "tfeiszt/sql-builder": "dev-master"
  }
}
```

### Init SqlBuilder
```php
// composer autoload.php
require_once './vendor/autoload.php';

```

### SELECT queries
```php
$q = new SelectQuery('*');
$q->from('users')
    ->where()
    ->equal('user_name', 'tfeiszt')
    ->and()->equal('status', '1');

echo ($q->toSql(); //SQL text
var_dump($q->getArgs()); //Argument list

```
```sql
SELECT * FROM users WHERE user_name = ? AND status = ?
```

```php
array (size=2)
  0 => string 'tfeiszt' (length=5)
  1 => string '1' (length=8)
```
###Prepare and execute via PDO
```php
//Build query
$q = new SelectQuery('*');
$q->from('users')
    ->where()
    ->equal('user_name', 'tfeiszt')
    ->and()->equal('status', '1');
//Execute    
$db = new PDO('mysql:host=localhost;dbname=testdb;charset=utf8mb4', 'username', 'password');    
$stmt = $db->prepare($q->toSql()); //Prepare via PDO
$stmt->execute($q->getArgs()); //Exceute via PDO
```

### Simple SELECT query
```php
$q = new SelectQuery('u.*');
$q->from('users','u')
    ->join('user_roles', 'ur')
    ->on(new SqlString('u.id = ur.user_id'))
    ->on(new ArithmeticExpression('ur.role_id', '=', 'admin'));
$q->where()
    ->equal('u.city', 'London');
echo $q->orderBy('user_name')->page(10, 2)->toSql(); //SQL text
var_dump($q->getArgs()); //Argument list
```
```sql
SELECT u.* 
FROM users u 
  JOIN user_roles ur ON (u.id = ur.user_id AND ( ur.role_id = ?)) 
WHERE ( u.city = ?) 
  ORDER BY user_name 
  LIMIT ? OFFSET ?
```

```php
array (size=4)
  0 => string 'admin' (length=5)
  1 => string 'London' (length=6)
  2 => int 10
  3 => int 10
```

### GROUP BY, HAVING Condition
```php
$q = new SelectQuery();
$q->select(['first_name', new SqlCount()])
    ->from('users')
    ->groupBy('first_name')
    ->having(new SqlCount(), '>', 1);
echo $q->orderBy(new SqlCount(), 'DESC')->toSql(); //SQL text
var_dump($q->getArgs()); //Argument list
```

```sql
SELECT first_name, COUNT(*) 
FROM users 
  GROUP BY first_name 
  HAVING (( COUNT(*)) > ? )
  ORDER BY COUNT(*) DESC
```

```php
array (size=1)
    0 => int 1
```

### WHERE groups
```php
$q = new SelectQuery('*');
$q->from('users')
    ->where()
    ->append((new SqlConditions())->like('user_name', 'John') )
    ->append((new SqlConditions())->equal('1','1')->or()->equal('user_role', 'admin') );
echo $q->orderBy('user_name')->page(10, 1)->toSql();//SQL text
var_dump($q->getArgs()); //Argument list
```
```sql
SELECT * FROM users 
WHERE (
  ( user_name LIKE ? )
  ) AND 
  (
    ( 1 = ?) 
    OR 
    ( user_role = ?)
  ) 
  ORDER BY user_name 
  LIMIT ?
```
```php
array (size=4)
    0 => string '%John%' (length=6)
    1 => string '1' (length=1)
    2 => string 'admin' (length=5)
    3 => int 10
```
### UNION, UNION ALL Queries
```php
$q = new SelectQuery(['id', 'first_name', 'last_name', 'user_type']);
$q->from('users', 'u1')
    ->where()
    ->equal('u1.usert_type', 'admin');
$q2 = new SelectQuery(['id', 'first_name', 'last_name', 'user_type']);
$q2->from('users', 'u2')
    ->where()
    ->equal('u2.user_type', 'superadmin');
$q->union(
    $q2
)->all();
echo $q->orderBy('last_name')->toSql(); //SQL text
var_dump($q->getArgs()); //Argument list
```
```sql
SELECT id, first_name, last_name, user_type 
  FROM users u1 
WHERE ( u1.usert_type = ?) 
UNION ALL 
SELECT id, first_name, last_name, user_type 
  FROM users u2 
WHERE ( u2.user_type = ?) 
ORDER BY last_name
```

```php
array (size=2)
  0 => string 'admin' (length=5)
  1 => string 'superadmin' (length=10)
```
### EXISTS Select
```php
$partial = new SelectQuery('*');
$partial->from('user_roles')
    ->where()
    ->equal('role_id', 'admin')
    ->lessThan('role_expire', new SqlString('NOW()'));
$q = new DeleteQuery();    
$q->delete('users')
    ->where()
    ->equal('user_status', 1)
    ->exists($partial);
echo $q->toSql(); //SQL text
var_dump($q->getArgs()); //Argument list
```

```sql
DELETE FROM users 
WHERE ( user_status = ?) 
  AND (
      EXISTS (SELECT * FROM user_roles 
                WHERE ( role_id = ?) 
                AND ( role_expire < (NOW()))
              )
  )
```
```php
array (size=2)
    0 => int 1
    1 => string 'admin' (length=5)
```
### Dynamic view
```php
$view = new SelectQuery('*');
$view->from('users')
    ->where()
    ->equal('status', 1);
$q = new SelectQuery('*');
$q->from($view, 'v_users')
    ->join('user_roles')
    ->on(new SqlString('v_users.id = user_roles.user_id'))
    ->on(new ArithmeticExpression('user_roles.role_type','=', 'admin'));
$q->where()
    ->like('first_name', 'John');
echo $q->orderBy('last_name')->toSql();
var_dump($q->getArgs());
```
```sql
SELECT * FROM 
  (SELECT * FROM users 
      WHERE ( status = ?)
  ) v_users 
  JOIN user_roles ON (v_users.id = user_roles.user_id 
    AND ( user_roles.role_type = ?)) 
WHERE ( first_name LIKE ? ) 
  ORDER BY last_name
```

```php
array (size=3)
  0 => int 1
  1 => string 'admin' (length=5)
  2 => string '%John%' (length=6)
 ```
 
### License

MIT
